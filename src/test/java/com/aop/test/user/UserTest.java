package com.aop.test.user;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.aop.test.base.BaseTest;
import com.aop.test.model.User;
import com.aop.test.service.IUserService;

public class UserTest extends BaseTest{
	
	@Resource
	private IUserService userService;
	
	@Test
	public void testSelectId() {
		String result = JSONObject.toJSONString(userService.selectById(1));
		System.out.println("@@@@@@@@@@@@" + result);
	}
	
	@Test
	public void testInsert() {
		User entity = new User();
		entity.setName("bobo");
		entity.setAge(18);
		boolean result = userService.insert(entity);
		Assert.assertTrue(result);
	}
}
