package com.aop.test.mapper;

import com.aop.test.model.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author xiaoxb
 * @since 2017-11-23
 */
public interface UserMapper extends BaseMapper<User> {

}