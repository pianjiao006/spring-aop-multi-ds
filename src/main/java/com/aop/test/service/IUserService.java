package com.aop.test.service;

import com.aop.test.model.User;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiaoxb
 * @since 2017-11-23
 */
public interface IUserService extends IService<User> {
	
	public User selectOne(Integer id);
	
}
