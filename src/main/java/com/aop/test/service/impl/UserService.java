package com.aop.test.service.impl;

import com.aop.test.model.User;
import com.aop.test.ds.annotation.DataSource;
import com.aop.test.mapper.UserMapper;
import com.aop.test.service.IUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiaoxb
 * @since 2017-11-23
 */
@Service
public class UserService extends ServiceImpl<UserMapper, User> implements IUserService {

	@Override
	@DataSource("read")
	public User selectOne(Integer id) {
		return super.selectById(id);
	}

}
