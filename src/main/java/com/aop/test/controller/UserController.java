package com.aop.test.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aop.test.common.controller.BaseController;
import com.aop.test.service.IUserService;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xiaoxb
 * @since 2017-11-23
 */
@Controller
@RequestMapping("/user")
public class UserController extends BaseController {
	
	@Resource
	private IUserService userService;
	
	@ResponseBody
	@RequestMapping("selectOne")
	public Object selectUserById() {
		return userService.selectById(1);
	}
}
